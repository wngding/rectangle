import './rectangle.css';

const q = document.querySelector,
      $ = q.bind(document);

import { Rectangle, validate, isLegalKey } from './calc.js';

let $width = $('#width'),
    $height = $('#height'),
    $form = $('form'),
    $perimeter = $('#perimeter'),
    $area = $('#area'),
    $widthValidate = $('#width-validate'),
    $heightValidate = $('#height-validate'),
    isPassValidate = false;

$form.onsubmit = (e) => {
  e.preventDefault();
  if(!isPassValidate) return;

  let r = new Rectangle($width.value, $height.value);

  $perimeter.value = r.perimeter;
  $area.value = r.area;
};

$width.onkeypress = (e) => {
  if(!isLegalKey(e.key, e.target.value, e.target.selectionStart)) {
    e.preventDefault();
  }
};

$height.onkeypress = (e) => {
  if(!isLegalKey(e.key, e.target.value, e.target.selectionStart)) {
    e.preventDefault();
  }
};

$width.onblur = () => {
  const result = validate($width.value);
  isPassValidate = result.isOK;
  if(!result.isOK) {
    $widthValidate.innerText = '宽度' + result.reason;
    $width.select();
  } else {
    $widthValidate.innerText = '';
  }
};

$height.onblur = () => {
  const result = validate($height.value);
  isPassValidate = result.isOK;
  if(!result.isOK) {
    $heightValidate.innerText = '高度' + result.reason;
    $height.select();
  } else {
    $heightValidate.innerText = '';
  }
};
