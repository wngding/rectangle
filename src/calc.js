export class Rectangle {
  constructor(width, height) {
    this.#w = Number(width);
    this.#h = Number(height);
  }

  get area() {
    return this.#cut(this.#w * this.#h);
  }

  get perimeter() {
    return this.#cut(2 * (this.#w + this.#h));
  }

  #cut(x, n=2) {
    return Math.round(x * Math.pow(10, n)) / Math.pow(10, n);
  }

  #w = 0;
  #h = 0;
}

export function validate(data) {
  const result = {
    isOK: false,
    reason: ''
  };

  if(data === '') {
    result.reason = '不能为空！';
    return result;
  }

  if(!/^-?(0|[1-9]\d*)(\.\d*)?([eE][+-]?\d+)?$/.test(data)) {
    result.reason = '必须是数值';
    return result;
  }

  if(Number(data) < 0) {
    result.reason = '必须大于零';
    return result;
  }

  result.isOK = true;
  return result;
}

export function isLegalKey(key, content, pos) {
  // 过滤非法字符
  if(/[abcdf-zABCDF-Z`~!@#$%^&*()=_+[\]{}|;:'",<>/?\\]/.test(key)) {
    return false;
  }

  // 合法字符：- 负号
  if(key === '-') {
    // 规则：负号不能出现在数字的首位
    if(pos === 0) return false;

    // 规则：负号不能重复出现
    if(content.indexOf('-') !== -1 ) return false;

    // 规则：负号只能出现在 e|E 后面
    if(content.substring(0, pos).indexOf('e') === -1
        && content.substring(0, pos).indexOf('E') === -1) return false;
  }

  // 合法字符：. 小数点
  if(key === '.') {
    // 规则：小数点不能出现在数字的首位
    if(pos === 0) return false;
    
    // 规则：小数点不能重复出现
    if(content.indexOf('.') !== -1) return false;

    // 规则：小数点不能跟在负号后面
    if(pos === 1 && content.substring(0,1) === '-') return false;

    // 规则：小数点不能出现在 e|E 后面
    if((content.indexOf('E') !== -1)
      && (pos >= content.indexOf('E'))) return false;

    if((content.indexOf('e') !== -1)
      && (pos >= content.indexOf('e'))) return false;
  }

  // 合法字符：e 和 E 科学计数法指数符号
  if(key === 'e' || key === 'E') {
    // 规则：e 和 E 不能出现在数字的首位
    if(pos === 0) return false;

    // 规则：e 和 E 不能重复出现
    if(content.indexOf('e') !== -1 
        || content.indexOf('E') !== -1) return false;

    // 规则：e 和 E 不能跟在负号后面
    if(pos === 1 && content.substring(0,1) === '-') return false;

    // 规则：e 和 E 不能出现在小数点前面
    if(pos < content.indexOf('.')) return false;
  }

  return true;
}
