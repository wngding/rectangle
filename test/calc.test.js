/* global describe Rectangle validate isLegalKey test: true */
import { Rectangle, validate, isLegalKey } from '../src/calc.js';

describe('area() 测试', () =>{
  test('宽度和高度是整数', () =>{
    let r = new Rectangle(4, 5);
    expect(r.area).toBe(20);
  });

  test('宽度和高度是小数', () => {
    let r = new Rectangle(0.1, 0.2);
    expect(r.area).toBe(0.02);
  });

  test('宽度和高度是整数字符串', () => {
    let r = new Rectangle('4', '5');
    expect(r.area).toBe(20);
  });

  test('宽度和高度都是特殊值 0', () => {
    let r = new Rectangle(0, 0);
    expect(r.area).toBe(0);
  });

  test('宽度和高度都是非法字符串', () =>{
    let r = new Rectangle('a', 'b');
    expect(r.area).toBeNaN();
  });
});

describe('perimeter() 测试', () =>{
  test('宽度和高度是整数', () =>{
    let r = new Rectangle(4, 5);
    expect(r.perimeter).toBe(18);
  });

  test('宽度和高度是小数', () => {
    let r = new Rectangle(0.1, 0.2);
    expect(r.perimeter).toBe(0.6);
  });

  test('宽度和高度是整数字符串', () => {
    let r = new Rectangle('4', '5');
    expect(r.perimeter).toBe(18);
  });

  test('宽度和高度都是特殊值 0', () => {
    let r = new Rectangle(0, 0);
    expect(r.perimeter).toBe(0);
  });

  test('宽度和高度都是非法字符串', () =>{
    let r = new Rectangle('a', 'b');
    expect(r.perimeter).toBeNaN();
  });
});

describe('validate() 测试', function() {
  test('零是合法数据', function() {
    const result = validate('0');
    expect(result.isOK).toBeTruthy();
  });

  test('正小数是合法数据', function() {
    const result = validate('4.5');
    expect(result.isOK).toBeTruthy();
  });

  test('正整数是合法数据', function() {
    const result = validate('45');
    expect(result.isOK).toBeTruthy();
  });

  test('e 科学计数法是合法数据', function() {
    const result = validate('3.4e3');
    expect(result.isOK).toBeTruthy();
  });

  test('E 科学计数法是合法数据', function() {
    const result = validate('3.4E3');
    expect(result.isOK).toBeTruthy();
  });

  test('空数据是非法数据', function() {
    const result = validate('');
    expect(result.isOK).toBeFalsy();
  });

  test('非数值字符串是非法数据', function() {
    const result = validate('abc');
    expect(result.isOK).toBeFalsy();
  });

  test('字母数字混合是非法数据', function() {
    const result = validate('abc123');
    expect(result.isOK).toBeFalsy();
  });

  test('标点符号数字混合是非法数据', function() {
    const result = validate('%^134');
    expect(result.isOK).toBeFalsy();
  });

  test('负数是非法数据', function() {
    const result = validate('-44');
    expect(result.isOK).toBeFalsy();
  });
});

describe('isLegalKey() 测试', function() {
  describe('合法字符：5', function() {
    test('允许出现在空文本中', function() {
      expect(isLegalKey('5', '', 0)).toBeTruthy();
    });

    test('允许出现在合法数据前', function() {
      expect(isLegalKey('5', '3.141', 0)).toBeTruthy();
    });

    test('允许出现在合法数据中间', function() {
      expect(isLegalKey('5', '3.141', 2)).toBeTruthy();
    });

    test('允许出现在合法数据末尾', function() {
      expect(isLegalKey('5', '3.141', 5)).toBeTruthy();
    });
  });

  describe('合法字符：.', function() {
    test('允许出现在整数末尾', function() {
      expect(isLegalKey('.', '3141', 4)).toBeTruthy();
    });

    test('允许出现在整数第一位数字后的任意位置', function() {
      expect(isLegalKey('.', '3141', 1)).toBeTruthy();
    });

    test('不允许出现在整数前', function() {
      expect(isLegalKey('.', '3141', 0)).toBeFalsy();
    });

    test('不允许出现在空文本中', function() {
      expect(isLegalKey('.', '', 0)).toBeFalsy();
    });

    test('不允许出现在负号后', function() {
      expect(isLegalKey('.', '-', 1)).toBeFalsy();
    });

    test('不允许出现在小数中', function() {
      expect(isLegalKey('.', '3.141', 5)).toBeFalsy();
    });

    test('不允许出现在科学计数法的指数中', function() {
      expect(isLegalKey('.', '3e141', 3)).toBeFalsy();
    });
  });

  describe('合法字符：e', function() {
    test('允许出现在非科学计数法的合法数字末尾', function() {
      expect(isLegalKey('e', '3', 1)).toBeTruthy();
    });

    test('允许出现在非科学计数法的合法数字中间', function() {
      expect(isLegalKey('e', '3.141', 3)).toBeTruthy();
    });

    test('不允许出现在非科学计数法的合法数字前', function() {
      expect(isLegalKey('e', '3.141', 0)).toBeFalsy();
    });

    test('不允许出现在空文本中', function() {
      expect(isLegalKey('e', '', 0)).toBeFalsy();
    });

    test('不允许出现在负号后', function() {
      expect(isLegalKey('e', '-', 1)).toBeFalsy();
    });

    test('不允许出现在科学计数法数字的末尾', function() {
      expect(isLegalKey('e', '3.14e2', 6)).toBeFalsy();
    });

    test('不允许出现在科学计数法数字的中间', function() {
      expect(isLegalKey('e', '3.14e2', 4)).toBeFalsy();
    });

    test('不允许出现在科学计数法数字的前面', function() {
      expect(isLegalKey('e', '3.14e2', 0)).toBeFalsy();
    });

    test('不允许出现在 E 科学计数法数字的末尾', function() {
      expect(isLegalKey('e', '3.14E2', 6)).toBeFalsy();
    });

    test('不允许出现在 E 科学计数法数字的中间', function() {
      expect(isLegalKey('e', '3.14E2', 4)).toBeFalsy();
    });

    test('不允许出现在 E 科学计数法数字的前面', function() {
      expect(isLegalKey('e', '3.14E2', 0)).toBeFalsy();
    });

    test('不允许出现在小数点前面', function() {
      expect(isLegalKey('e', '123.456', 2)).toBeFalsy();
    });
  });

  describe('合法字符：E', function() {
    test('允许出现在非科学计数法的合法数字末尾', function() {
      expect(isLegalKey('E', '3.141', 5)).toBeTruthy();
    });

    test('允许出现在非科学计数法的合法数字中间', function() {
      expect(isLegalKey('E', '3.141', 3)).toBeTruthy();
    });

    test('不允许出现在非科学计数法的合法数字前', function() {
      expect(isLegalKey('E', '3.141', 0)).toBeFalsy();
    });

    test('不允许出现在空文本中', function() {
      expect(isLegalKey('E', '', 0)).toBeFalsy();
    });

    test('不允许出现在负号后', function() {
      expect(isLegalKey('E', '-', 1)).toBeFalsy();
    });

    test('不允许出现在科学计数法数字的末尾', function() {
      expect(isLegalKey('E', '3.14E2', 6)).toBeFalsy();
    });

    test('不允许出现在科学计数法数字的中间', function() {
      expect(isLegalKey('E', '3.14E2', 4)).toBeFalsy();
    });

    test('不允许出现在科学计数法数字的前面', function() {
      expect(isLegalKey('E', '3.14E2', 0)).toBeFalsy();
    });

    test('不允许出现在 e 科学计数法数字的末尾', function() {
      expect(isLegalKey('E', '3.14e2', 6)).toBeFalsy();
    });

    test('不允许出现在 e 科学计数法数字的中间', function() {
      expect(isLegalKey('E', '3.14e2', 4)).toBeFalsy();
    });

    test('不允许出现在 e 科学计数法数字的前面', function() {
      expect(isLegalKey('E', '3.14e2', 0)).toBeFalsy();
    });

    test('不允许出现在小数点前面', function() {
      expect(isLegalKey('E', '123.456', 2)).toBeFalsy();
    });
  });

  describe('合法字符：-', function() {
    test('不允许出现在非科学计数法的合法数字末尾', function() {
      expect(isLegalKey('-', '3.141', 5)).toBeFalsy();
    });

    test('不允许出现在非科学计数法的合法数字中间', function() {
      expect(isLegalKey('-', '3.141', 3)).toBeFalsy();
    });

    test('不允许出现在非科学计数法的合法数字前', function() {
      expect(isLegalKey('-', '3.141', 0)).toBeFalsy();
    });

    test('不允许出现在空文本中', function() {
      expect(isLegalKey('-', '', 0)).toBeFalsy();
    });

    test('不允许出现在负号后', function() {
      expect(isLegalKey('-', '-', 1)).toBeFalsy();
    });

    test('不允许出现在科学计数法数字的中间', function() {
      expect(isLegalKey('-', '3.14E2', 4)).toBeFalsy();
    });

    test('允许出现在 E 后面', function() {
      expect(isLegalKey('-', '3.14E2', 5)).toBeTruthy();
    });

    test('允许出现在 e 后面', function() {
      expect(isLegalKey('-', '3.14e2', 5)).toBeTruthy();
    });

    test('不允许重复出现', function() {
      expect(isLegalKey('-', '3.14e-2', 3)).toBeFalsy();
    });

    test('不允许出现在小数点后面', function() {
      expect(isLegalKey('-', '123.456', 4)).toBeFalsy();
    });
  });

  describe('非法字母：c', function() {
    test('不允许出现在空文本中', function() {
      expect(isLegalKey('c', '', 0)).toBeFalsy();
    });

    test('不允许出现在合法数字前', function() {
      expect(isLegalKey('c', '3.141', 0)).toBeFalsy();
    });

    test('不允许出现在合法数字中间', function() {
      expect(isLegalKey('c', '3.141', 3)).toBeFalsy();
    });

    test('不允许出现在合法数字末尾', function() {
      expect(isLegalKey('c', '3.141', 5)).toBeFalsy();
    });
  });

  describe('非法字母：X', function() {
    test('不允许出现在空文本中', function() {
      expect(isLegalKey('X', '', 0)).toBeFalsy();
    });

    test('不允许出现在合法数字前', function() {
      expect(isLegalKey('X', '3141', 0)).toBeFalsy();
    });

    test('不允许出现在合法数字中间', function() {
      expect(isLegalKey('X', '3141', 3)).toBeFalsy();
    });

    test('不允许出现在合法数字末尾', function() {
      expect(isLegalKey('X', '3141', 4)).toBeFalsy();
    });
  });

  describe('非法符号：%', function() {
    test('不允许出现在空文本中', function() {
      expect(isLegalKey('%', '', 0)).toBeFalsy();
    });

    test('不允许出现在合法数字前', function() {
      expect(isLegalKey('%', '3.1e4', 0)).toBeFalsy();
    });

    test('不允许出现在合法数字中间', function() {
      expect(isLegalKey('%', '3.1e4', 3)).toBeFalsy();
    });

    test('不允许出现在合法数字末尾', function() {
      expect(isLegalKey('%', '3.1e4', 5)).toBeFalsy();
    });
  });
});
